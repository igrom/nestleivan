@basketItems

Feature:  Basket management (Add / Delete)

Scenario: Scenario Documentation

  """
  Documentation of feature for add remove from Basket
  Basic Happy path only were added
  Sad paths need to be consider, i.e:
  - No amazon items for the given products
  - Amazon button is not working properly
  - Items is not added to the basket
  - Items are not removed from basket
  Notes: Background can be added for reducing steps repetition and data loaded
  Data test should be added in java steps to get value for each test

  """

  Scenario Outline: Successful product listed in Amazon
    Given  Amazon web portal up and running
    When Amazon is launched
    And the product "<product>" is selected
    Then the list products are displayed

    Examples:
    | product |
    | cable   |
    | iphone  |

  Scenario Outline: List the items from a web search
    Given The product to search contains the word "<product>"
    And  Amazon has products which contains the product "<product>"
    When I search for the given product "<product>"
    Then I can see only product related to the given product "<product>"

  Examples:
  | product |
  | cable   |
  | iphone  |

  Scenario Outline: Search and filter results by type and price
    Given The product to search contains the word "<product>"
    And  Amazon has products which contains the product "<product>"
    When I click order dropbown button
    And I filter results by "<orderType>"
    Then I can see only product related to the given product and ordered by "<orderType>"
    
  Examples:
  | product | orderType |
  | cable   | lower price |
  | iphone  | lower pirce |

  Scenario Outline: Add item to the basket
    Given The product to search contains the word "<product>"
    And  Amazon has products which contains the product "<product>"
    When I click order dropbown button
    And I filter results by "<orderType>"
    And I select a product to the basket
    Then The product is added to the basket
    And a message is displayed in Amazon with product details
    And the basket shows the item in the basket

  Examples:
  | product |
  | cable   |
  | iphone  |

  Scenario: Remove item from the basket
    Given the basket contains at least 1 product
    And I remove the product from the basket
    Then The product is deleted from the basket
    And the basket shows 0 products
