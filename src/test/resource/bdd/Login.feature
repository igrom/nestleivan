@login
Feature: Login to Amazon web page

Scenario: Documentation
  """
  Documentation of feature for Amazon login to https://www.amazon.es/
  Business wording will be added accordingly to example one with character ~

  """

  Scenario: Successful login to Amazon
    Given ~ Amazon web portal up and running
      Given  Amazon web portal up and running
    When ~ Amazon is launched
      When Amazon is launched
    Then ~ Amazon web portal displays items
      Then Amazon web portal displays items


    /*Sad Paths*/

  Scenario: Amazon Error "<errorCode>" when launching Amazon
    Given  Amazon web portal can be accessed
    And Amazon server are busy retuning timeout
    When Amazon web is launched
    Then an Amazon server error "<errorCode>" with description "<errorDescription>" is shown
    And No items are displayed

    Examples:
      | errorCode | errorDescription |
      | 500       | Internal server error |
      | 503       | Service Unavailable |
