package stepsDef;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(tags = {"@ivg"}, plugin = {"pretty", "html:target/cucumber"})
public class RunCukesTest {
}